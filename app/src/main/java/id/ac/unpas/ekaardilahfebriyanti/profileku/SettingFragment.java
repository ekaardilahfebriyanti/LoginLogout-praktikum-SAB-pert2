package id.ac.unpas.ekaardilahfebriyanti.profileku;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by admin601 on 2/28/2018.
 */

public class SettingFragment extends Fragment {
//    Button logout;
    public static MainActivity mainActivity;
    public static SettingFragment newInstance (MainActivity activity){
        mainActivity = activity;
        return new SettingFragment();
    }
    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle saveInstanceState){
        View view = LayoutInflater.from(mainActivity).inflate(R.layout.fragment_setting, container, false);

        return view;
    }
}
